# README

## What is Riverline\PredaddyBundle

``Riverline\PredaddyBundle`` help to integrate predaddy components into a Symfony2 application.

## Requirements

* PHP 5.3
* Symfony 2.x

## Installation

``Riverline\PredaddyBundle`` is compatible with composer and any prs-0 autoloader

## Configuration

```yml
riverline_predaddy:
    command_bus:
        name:  your-event-bus-name
        alias: your.cqrs.predaddy.command_bus
        annotation_reader: annotation_reader              # Annotation reader service id (default value)
        transaction_manager: predaddy.transaction_manager # Transaction manager service id (default value)

    event_bus:
        name:  your-event-bus
        alias: your.cqrs.predaddy.command_bus
        annotation_reader: annotation_reader              # Annotation reader service id (default value)
        transaction_manager: predaddy.transaction_manager # Transaction manager service id (default value)

    transaction_manager:
        class: trf4php\doctrine\DoctrineTransactionManager # (default value)
        entity_manager: doctrine.orm.entity_manager        # Doctrine entity manager service id (default value)
```

## Logging

``Riverline\PredaddyBundle`` add Symfony2 'logger' service as root logger (supports only Monolog logger).

## Usage

Define command and event handler as container service, and add tag to register to buses.

```yml
services:

  ## EVENT HANDLERS
    my.cqrs.event_handler.something:
      class: My\Vendor\SomethingEventHandler
      tags:
        -  { name: predaddy.event_handler }

  ## COMMAND HANDLERS
    my.cqrs.command_handler.something:
      class: My\Vendor\SomethingEventHandler
      tags:
        -  { name: predaddy.command_handler }
```

## Limitations / Known issues

* Works only with Predaddy 1.2
* Not tested
* Supports only Monolog logger
* And probably other ...

## License

See Resources/meta/LICENSE
