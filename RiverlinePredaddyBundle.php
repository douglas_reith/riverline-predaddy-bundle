<?php

namespace Riverline\PredaddyBundle;

use lf4php\LoggerFactory;
use Riverline\PredaddyBundle\DependencyInjection\Compiler\HandlerCompilerPass;
use Riverline\PredaddyBundle\Logging\MonologLoggerFactory;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RiverlinePredaddyBundle extends Bundle
{

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        parent::boot();

        LoggerFactory::setILoggerFactory(new MonologLoggerFactory($this->container->get('logger')));
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new HandlerCompilerPass(), PassConfig::TYPE_AFTER_REMOVING);
    }

}
