<?php

namespace Riverline\PredaddyBundle\MessageHandling;

use Exception;
use Psr\Log\LoggerInterface;

class LoggerMessageCallback extends CommandMessageCallback
{

    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function onSuccess($result)
    {
        parent::onSuccess($result);

        if (is_scalar($result)) {
            $this->logger->debug("[LoggerMessageCallback::onSuccess()] (scalar) Result = $result");
        } else if (is_array($result)) {
            $this->logger->debug("[LoggerMessageCallback::onSuccess()] (array) Result has ".count($result)." elements");
            foreach($result as $key => $value) {
                $this->logger->debug("[LoggerMessageCallback::onSuccess()] Result[{$key}] = ".gettype($value));
            }
        } else if (is_object($result)) {
            $this->logger->debug("[LoggerMessageCallback::onSuccess()] (object) Result class = ".get_class($result));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function onFailure(Exception $exception)
    {
        parent::onFailure($exception);

        $this->logger->error("[LoggerMessageCallback::onFailure()] Exception message = {$exception->getMessage()}");
    }

} 