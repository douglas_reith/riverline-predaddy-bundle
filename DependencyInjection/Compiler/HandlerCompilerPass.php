<?php

namespace Riverline\PredaddyBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class HandlerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // Command handlers
        $commandBusDefinition = $container->getDefinition('predaddy.command_bus');

        foreach ($container->findTaggedServiceIds('predaddy.command_handler') as $id => $attributes) {
            $commandBusDefinition->addMethodCall(
                'register',
                array(new Reference($id))
            );
        }

        // Event handlers
        $eventBusDefinition = $container->getDefinition('predaddy.event_bus');

        foreach ($container->findTaggedServiceIds('predaddy.event_handler') as $id => $attributes) {
            $eventBusDefinition->addMethodCall(
                'register',
                array(new Reference($id))
            );
        }
    }
} 