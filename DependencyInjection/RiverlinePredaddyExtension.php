<?php

namespace Riverline\PredaddyBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class RiverlinePredaddyExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        // Command bus
        if ($config['command_bus']['annotation_reader'] != 'annotation_reader') {
            $annotatedMessageHandlerDescriptorFactoryDefinition = $container->findDefinition('predaddy.command_message_handler_descriptor_factory');
            $annotatedMessageHandlerDescriptorFactoryDefinition->replaceArgument(1,  new Reference($config['command_bus']['annotation_reader']));
        }

        $commandBusDefinition = $container->findDefinition('predaddy.command_bus');
        $commandBusDefinition->replaceArgument(0, $config['command_bus']['name']);
        if (! empty($config['command_bus']['alias'])) {
            $container->setAlias($config['command_bus']['alias'], 'predaddy.command_bus');
        }
        if ($config['command_bus']['transaction_manager'] != 'predaddy.transaction_manager') {
            $commandBusDefinition->replaceArgument(3,  new Reference($config['command_bus']['transaction_manager']));
        }

        // Event bus
        if ($config['event_bus']['annotation_reader'] != 'annotation_reader') {
            $annotatedMessageHandlerDescriptorFactoryDefinition = $container->findDefinition('predaddy.event_message_handler_descriptor_factory');
            $annotatedMessageHandlerDescriptorFactoryDefinition->replaceArgument(1,  new Reference($config['event_bus']['annotation_reader']));
        }

        $eventBusDefinition = $container->findDefinition('predaddy.event_bus');
        $eventBusDefinition->replaceArgument(0, $config['event_bus']['name']);
        if (! empty($config['event_bus']['alias'])) {
            $container->setAlias($config['event_bus']['alias'], 'predaddy.event_bus');
        }
        if ($config['event_bus']['transaction_manager'] != 'predaddy.transaction_manager') {
            $eventBusDefinition->replaceArgument(3,  new Reference($config['event_bus']['transaction_manager']));
        }

        // Transaction manager
        $transactionManagerDefinition = $container->findDefinition('predaddy.transaction_manager');
        $container->setParameter('predaddy.transaction_manager.class', $config['transaction_manager']['class']);
        if ($config['transaction_manager']['entity_manager'] != 'doctrine.orm.entity_manager') {
            $transactionManagerDefinition->replaceArgument(0,  new Reference($config['transaction_manager']['entity_manager']));
        }

        // Logging
        if ($config['logging']['type'] != 'monolog') {
            throw new \RuntimeException("Unmanaged logging type {$config['logging']['type']}");
        }
    }
}
