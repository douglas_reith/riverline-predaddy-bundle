<?php

namespace Riverline\PredaddyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('riverline_predaddy');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->arrayNode('command_bus')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('name')->defaultValue('command-bus')->end()
                        ->scalarNode('alias')->defaultNull()->end()
                        ->scalarNode('annotation_reader')->defaultValue('annotation_reader')->end()
                        ->scalarNode('transaction_manager')->defaultValue('predaddy.transaction_manager')->end()
                    ->end()
                ->end()
                ->arrayNode('event_bus')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('name')->defaultValue('event-bus')->end()
                        ->scalarNode('alias')->defaultNull()->end()
                        ->scalarNode('annotation_reader')->defaultValue('annotation_reader')->end()
                        ->scalarNode('transaction_manager')->defaultValue('predaddy.transaction_manager')->end()
                    ->end()
                ->end()
                ->arrayNode('transaction_manager')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('class')->defaultValue('trf4php\doctrine\DoctrineTransactionManager')->end()
                        ->scalarNode('entity_manager')->defaultValue('doctrine.orm.entity_manager')->end()
                    ->end()
                ->end()
                ->arrayNode('logging')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('type')->defaultValue('monolog')->end()
                        ->scalarNode('monolog_logger')->defaultValue('logger')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
